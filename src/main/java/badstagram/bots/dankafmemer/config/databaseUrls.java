package badstagram.bots.dankafmemer.config;

public class databaseUrls {

    public static final String GUILD_CONFIG_URL  = "jdbc:sqlite:db/guildConfig.db";
    public static final String PUNISHMENT_URL    = "jdbc:sqlite:db/punishments.db";
    public static final String COMMANDS_URL      = "jdbc:sqlite:db/commands";
    public static final String USER_INFO_URL     = "jdbc:sqlite:db/userInfo.db";

}
