package badstagram.bots.dankafmemer.config;

import com.jagrosh.jdautilities.commons.JDAUtilitiesInfo;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDAInfo;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.PermissionOverride;

import java.util.*;

public class config {
    public static final String BOT_VERSION = "ALPHA_151119_00";
    public static final String JDA_VERSION = JDAInfo.VERSION;
    public static final String JDA_UTILS_VERSION = JDAUtilitiesInfo.VERSION;

    public static final Map<Long, PermissionOverride> oldPermissions = new HashMap<>();
    public static Guild.VerificationLevel oldVerificationLevel = null;

    private static final Permission[] PERMISSIONS = {
            Permission.MESSAGE_EMBED_LINKS,
            Permission.MESSAGE_READ,
            Permission.MESSAGE_WRITE,
            Permission.KICK_MEMBERS,
            Permission.BAN_MEMBERS,
            Permission.CREATE_INSTANT_INVITE,
            Permission.MESSAGE_HISTORY,
    };


    public static List<String> copypastas = new LinkedList<>();

    public static String generateInviteLink(JDA jda) {
        return String.format("https://discordapp.com/oauth2/authorize?client_id=%s&scope=bot&permissions=%s", jda.getSelfUser().getId(), Permission.getRaw(PERMISSIONS));
    }

    public static List<String> disabledCommands = new ArrayList<>();

    public static String[] args;
}
