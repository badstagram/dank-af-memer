package badstagram.bots.dankafmemer.main;

import badstagram.bots.dankafmemer.config.config;
import badstagram.bots.dankafmemer.login.login;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import sun.net.www.protocol.http.AuthenticationInfo;

public class main {


    public static void main(String[] args) {
        EventWaiter waiter = new EventWaiter();

        config.args = args;

        try {
            login.startBot(waiter, args[0]);
        } catch (Exception e) {
            utils.logErrorToSentry(e);
            e.printStackTrace();
        }

    }
}
