package badstagram.bots.dankafmemer.listeners;

import badstagram.bots.dankafmemer.automod.automod;
import badstagram.bots.dankafmemer.automod.serverSecurity;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.util.EnumSet;

public class messageListener extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent event) {
        EnumSet<Permission> userPerms = event.getMember().getPermissions();
        boolean canByBypass = userPerms.contains(Permission.MANAGE_SERVER) | userPerms.contains(Permission.ADMINISTRATOR);
        if (!event.getAuthor().isBot() && !canByBypass) {
            automod.antiAdvertise(event.getMessage(), event.getGuild());

            serverSecurity.userBot(event.getMember(), event.getMessage(), event.getGuild(), event);
        }

    }
}
