package badstagram.bots.dankafmemer.listeners;

import badstagram.bots.dankafmemer.config.databaseUrls;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class userJoinListener extends ListenerAdapter {

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        Connection connection;
        Statement statement;
        ResultSet rs;
        Guild g = event.getGuild();

        try {
            connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
            statement = connection.createStatement();
            rs = statement.executeQuery(String.format("SELECT 'raid_mode' FROM '%s'", g.getId()));

            boolean raidmodeActive = rs.getBoolean("raid_mode");
            if (raidmodeActive) {
                User u = event.getUser();
                u.openPrivateChannel().queue((channel) -> {
                    channel.sendMessageFormat("%s is currently under lockdown, please wait and try again.").queue();
                    g.kick(u.getId(), "Raidmode active").queue();

                });
            }

        } catch (Exception e) {
            EmbedBuilder embedToDev = new EmbedBuilder();
            embedToDev.setTitle("A fatal error occurred!");
            embedToDev.addField("Exception message", e.getMessage(), false);
            StringBuilder sb = new StringBuilder();
            for (StackTraceElement ste : e.getStackTrace()) {
                sb.append(ste.toString());
                sb.append("\n");
            }
            embedToDev.addField("Stack Trace", sb.toString().substring(1, 1024), false);
            embedToDev.addField("Command", "raidmode.userJoin", false);
            embedToDev.setFooter(String.format("Guild: %s (%s)", g.getName(), g.getId()), g.getIconUrl());
            embedToDev.setColor(new Color(255, 0, 0));
            g.getJDA().getGuildsByName("dank af memer", true).get(0).getTextChannelsByName("error-reports", true).get(0).sendMessage(embedToDev.build()).queue();
        }
    }
}
