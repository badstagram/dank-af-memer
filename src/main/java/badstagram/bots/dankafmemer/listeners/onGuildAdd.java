package badstagram.bots.dankafmemer.listeners;

import badstagram.bots.dankafmemer.database.databaseTools;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class onGuildAdd extends ListenerAdapter {
    public void onGuildJoin(@Nonnull GuildJoinEvent event) {
        databaseTools.createTable(event.getGuild().getId());

    }
}