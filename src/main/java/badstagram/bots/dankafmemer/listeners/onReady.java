package badstagram.bots.dankafmemer.listeners;


import badstagram.bots.dankafmemer.config.config;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public class onReady extends ListenerAdapter {


    private Logger logger = LoggerFactory.getLogger(onReady.class);

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        logger.info("Logged in!");

        logger.info(config.generateInviteLink(event.getJDA()));

        for (Guild g : event.getJDA().getGuildCache()) {
            logger.info(String.format("Guild %s (%s) loaded", g.getName(), g.getId()));
        }

        if (event.getJDA().getSelfUser().getId().equals("597877235017318407")) {
            event.getJDA().getPresence().setActivity(Activity.playing(String.format("Version: %s", config.BOT_VERSION)));
        } else if (event.getJDA().getSelfUser().getId().equals("639071511331995649")) {
            event.getJDA().getPresence().setActivity(Activity.playing("DEV BUILD"));
        }
    }
}
