package badstagram.bots.dankafmemer.database;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.utils;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;

import javax.annotation.Nonnull;
import java.sql.*;

public class databaseTools {

    //static
    static final String PUNISHMENT_URL = ""; //unused for now
    private static Connection guildConfigConnection = null;
    //Connection punishmentConnection = null


    /**
     * @param tableName the name for the newly made table
     */
    public static void createTable(@Nonnull String tableName) {
        try {
            guildConfigConnection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);

            try (Statement stmt = guildConfigConnection.createStatement()) {

                stmt.execute(String.format("create table `%s`\n" +
                        "(\n" +
                        "raidmode boolean default false not null,\n" +
                        "mutedRole text default 'not set' not null,\n" +
                        "punishLog text default 'not set' not null\n" +
                        ");", tableName));
            }
        } catch (SQLException e) {

            utils.logErrorToSentry(e);
        }
    }

    public static String getAmountBans(Connection connection, Message message, Guild g, String command) {
        Statement stmt = null;
        try {

            stmt = connection.createStatement();
            String sql = "SELECT `";
            ResultSet rs = stmt.executeQuery(sql);
        } catch (Exception e) {
            utils.error(e, message, g, command);
            return "Error";
        }
        return "";
    }
}




