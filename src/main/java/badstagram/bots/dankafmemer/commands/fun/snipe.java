package badstagram.bots.dankafmemer.commands.fun;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class snipe extends Command {
    public snipe(Category category) {
        this.name = "snipe";
        this.help = "gets the content of a message";
        this.guildOnly = true;
        this.category = category;
        this.arguments = "<message id>";
        this.botPermissions = new Permission[]{
                Permission.MESSAGE_READ,
                Permission.MESSAGE_HISTORY,
                Permission.MESSAGE_EMBED_LINKS
        };

    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        long messageId = Long.parseLong(event.getArgs().split("//s+")[0]);
        EmbedBuilder eb = new EmbedBuilder();
        EmbedBuilder failEmbed = new EmbedBuilder();
        event.getChannel().retrieveMessageById(messageId).queue((message -> {
            eb.setTitle("Snipe");
            eb.addField("Content", message.getContentRaw(), false);
            eb.addField("Author", message.getAuthor().getAsMention(), false);
            event.getChannel().sendMessage(eb.build()).queue();
        }), (throwable -> {
            failEmbed.setTitle("Snipe failed.");
            failEmbed.setDescription("Make sure you run the command in the same channel as the message.");
            event.getChannel().sendMessage(failEmbed.build()).queue();
        }));


    }
}
