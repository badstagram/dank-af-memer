package badstagram.bots.dankafmemer.commands.info;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class discordrep extends Command {
    int donator, reputation;
    boolean partner, admin, mod;

    public discordrep(Category category) {
        this.name = "discordrep";
        this.category = category;
        this.help = "Gets information about a user from discordrep";

    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        String userId = event.getAuthor().getId();
        OkHttpClient client = new OkHttpClient();
        URL url;
        Request request;
        Response response;
        String jsonData;
        JSONObject jsonObject;
        Logger logger = LoggerFactory.getLogger(discordrep.class);

        String bio = "";


        try {
            url = new URL(String.format("https://discordrep.com/api/u/%s?authorization=%s", userId, utils.getEnvVar("discord_rep_api_key")));

            request = new Request.Builder().url(url).build();
            response = client.newCall(request).execute();
            jsonData = response.body().string();
            jsonObject = new JSONObject(jsonData);

            bio = jsonObject.getString("bio");
            donator = jsonObject.getInt("donator");
            partner = jsonObject.getBoolean("partner");
            admin = jsonObject.getBoolean("admin");
            mod = jsonObject.getBoolean("mod");

            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle(String.format("Information about %s from discordrep", event.getAuthor().getAsTag()));
            eb.addField("Bio", MarkdownUtil.monospace(bio), true);
            eb.addField("Donator", convertDonator(donator), true);
            eb.addField("Partner", partner ? "Yes" : "No", true);
            eb.addField("Admin", admin ? "Yes" : "No", true);
            eb.addField("Mod", mod ? "Yes" : "No", true);
            eb.addField("reputation", getRep(event.getAuthor().getId(), event), true);
            //eb.addBlankField(true);
            event.reply(eb.build());


        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }

    }


    private String convertDonator(int donatorLevel) {
        switch (donatorLevel) {
            case 0:
                return "Not a donator";
            case 1:
                return "Tier I";
            case 2:
                return "Tier II";
            case 3:
                return "Tier III";
            case 4:
                return "Tier IV";
            case 5:
                return "Tier V";
            default:
                return "";
        }
    }

    private String getRep(String userId, CommandEvent event) {
        OkHttpClient client = new OkHttpClient();
        URL url;
        Request request;
        Response response;
        String jsonData;
        JSONObject jsonObject;
        try {
            url = new URL(String.format("https://discordrep.com/api/rep/%s?authorization=%s", userId, utils.getEnvVar("discord_rep_api_key")));

            request = new Request.Builder().url(url).build();

            response = client.newCall(request).execute();
            jsonData = response.body().string();

            jsonObject = new JSONObject(jsonData);

            reputation = jsonObject.getInt("reputation");

            return String.valueOf(reputation);

        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
        return "";
    }
}
