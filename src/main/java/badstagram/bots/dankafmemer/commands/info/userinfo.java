package badstagram.bots.dankafmemer.commands.info;

import badstagram.bots.dankafmemer.config.constants;
import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class userinfo extends Command {


    private Logger logger = LoggerFactory.getLogger(userinfo.class);

    public userinfo(Category category) {
        this.name = "userinfo";
        this.aliases = new String[]{"info"};
        this.category = category;
        this.help = "returns information about a user";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        String args = event.getArgs();
        Member m = null;

        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        if (mentionedMembers.isEmpty()) {
            m = event.getMessage().getMember();
        } else {
            m = mentionedMembers.get(0);
        }
        User u = m.getUser();

        List<Role> roles = m.getRoles();
        StringBuilder sb = new StringBuilder();

        for (Role r : roles) {
            sb.append(r.getAsMention()).append(" ");
        }


        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("User info for " + u.getAsTag());
        eb.setFooter(String.format("User Id: %s", u.getId()));
        StringBuilder descriptionBuilder = eb.getDescriptionBuilder();

        try {
            descriptionBuilder.append(MarkdownUtil.bold("Bans on record: ")).append(utils.getBans(event.getGuild(), u)).append("\n");
            descriptionBuilder.append(MarkdownUtil.bold("Kicks on record: ")).append(utils.getKicks(event.getGuild(), u)).append("\n");
            descriptionBuilder.append(MarkdownUtil.bold("Mutes on record: ")).append(utils.getMutes(event.getGuild(), u)).append("\n");
            descriptionBuilder.append(MarkdownUtil.bold("Warnings on record: ")).append(utils.getWarns(event.getGuild(), u)).append("\n");
            descriptionBuilder.append("\n");
            descriptionBuilder.append(isbannedOnDiscordRep(u.getId(), event) ? constants.ERROR + " User banned on DiscordRep" : constants.SUCCESS + " User not banned on DiscordRep").append("\n");
            descriptionBuilder.append(isBannedOnKsoft(u.getId(), event) ? constants.ERROR + " User banned on ksoft" : constants.SUCCESS + " User not banned on ksoft").append("\n");
            eb.setDescription(descriptionBuilder.toString());

            eb.addField("discordRep reputation", getDiscordRep(u.getId(), event), true);
            eb.addField(String.format("Server roles (%s)", roles.size()), sb.toString(), false);
            eb.addField("User flags", getUserFlags(u.getId(), event).equals("") ? "None" : getUserFlags(u.getId(), event), true);
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
        event.getTextChannel().sendMessage(eb.build()).queue();
    }

    private boolean isbannedOnDiscordRep(@Nonnull String userid, CommandEvent event) {
        int code;
        try {
            OkHttpClient client = new OkHttpClient();

            URL url = new URL(String.format("https://discordrep.com/api/bans/%s?authorization=%s", userid, utils.getEnvVar("discord_rep_api_key")));
            Request request = new Request.Builder()
                    .url(url)
                    .build();


            Response response;


            response = client.newCall(request).execute();
            code = response.code();

            logger.debug("code from discordRep: " + code);
            if (code != 200 && code != 404) {
                logger.warn(String.format("DiscordRep returned a non 200 response code: %s", code));

            }

        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
            return false;
        }
        if (code == 404) {
            return false;
        } else if (code == 200) {
            return true;
        } else return code != 427;
    }

    private String getDiscordRep(@Nonnull String userid, @Nonnull CommandEvent event) {
        try {
            OkHttpClient client = new OkHttpClient();
            HttpUrl url = new HttpUrl.Builder()
                    .scheme("https")
                    .host("discordrep.com")
                    .addPathSegments("api/rep")
                    .addPathSegment(userid)
                    .addQueryParameter("authorization", utils.getEnvVar("discord_rep_api_key"))
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();

            int code = response.code();
            logger.debug("code from discordRep: " + code);
            if (code != 200) {
                logger.warn(String.format("DiscordRep returned a non 200 response code: %s", code));
                return "0";
            }

            JSONObject jsonObject = new JSONObject(response.body().string());
            response.close();

            return String.valueOf(jsonObject.getInt("reputation"));

        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
            return "0";
        }
    }

    private boolean isBannedOnKsoft(@Nonnull String userId, @NotNull CommandEvent event) {
        OkHttpClient client = new OkHttpClient();
        HttpUrl httpUrl;
        Response response;
        Request request;

        try {

            httpUrl = new HttpUrl.Builder()
                    .scheme("https")
                    .host("api.ksoft.si")
                    .addPathSegment("bans")
                    .addPathSegment("check")
                    .addQueryParameter("user", userId)
                    .build();

            request = new Request.Builder()
                    .url(httpUrl)
                    .addHeader("Authorization", String.format("Bearer %s", utils.getEnvVar("ksoft")))
                    .build();

            response = client.newCall(request).execute();

            JSONObject jsonObject = new JSONObject(response.body().string());

            return jsonObject.getBoolean("is_banned");
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
            return false;
        }

    }

    private String getUserFlags(String userID, CommandEvent event) {

        Connection connection;
        Statement statement;
        ResultSet rs;
        String sql = "";

        try {

            connection = DriverManager.getConnection(databaseUrls.USER_INFO_URL);
            statement = connection.createStatement();
            sql = "SELECT flags FROM users WHERE user_id = " + userID;
            rs = statement.executeQuery(sql);

            while (rs.next()) {

                String[] flags = rs.getString("flags").split(", ");
                StringBuilder sb = new StringBuilder();
                for (String flag : flags) {
                    sb.append(MarkdownUtil.monospace(flag)).append(" ");
                }

                return sb.toString();
            }
            statement.close();
            rs.close();
            connection.close();

        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());

        }

        return "";
    }
}
