package badstagram.bots.dankafmemer.commands.info;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.utils.MarkdownUtil;

import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

public class roleInfo extends Command {
    public roleInfo(Category category) {
        this.name = "roleinfo";
        this.help = "Displays information about a role";
        this.category = category;
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        String args = event.getArgs();
        Role role;
        try {
            role = event.getGuild().getRolesByName(args, true).get(0);
        } catch (IndexOutOfBoundsException e) {
            event.getTextChannel().sendMessage("Role not found!").queue((msg) -> msg.delete().queueAfter(1, TimeUnit.MINUTES));
            return;
        }

        EnumSet<Permission> rolePermission = role.getPermissions();
        StringBuilder permissionBuilder = new StringBuilder();
        StringBuilder memberBuilder = new StringBuilder();


        EmbedBuilder eb = new EmbedBuilder();

        for (Permission perm : rolePermission) {
            permissionBuilder
                    .append(MarkdownUtil.monospace(perm.getName()))
                    .append(" ");
        }
        eb.setTitle(String.format("Role info for %s", role.getName()));
        eb.setColor(role.getColor());
        eb.addField("ID", role.getId(), true);

        int memberCount = 0;
        for (Member m : role.getGuild().getMembers()) {
            if (m.getRoles().contains(role)) {
                memberCount++;
                memberBuilder.append(m.getAsMention()).append(" ");
            }
        }


        eb.addField("Position", String.valueOf(role.getPosition()), true);
        eb.addField("Permissions", permissionBuilder.toString(), false);
        eb.addField(String.format("Members (%s)", memberCount), memberBuilder.toString(), false);


        event.reply(eb.build());
    }
}
