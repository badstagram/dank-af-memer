package badstagram.bots.dankafmemer.commands.info;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;

import java.util.List;
import java.util.Set;

public class guildInfo extends Command {
    public guildInfo(Category category) {
        this.name = "guildinfo";
        this.aliases = new String[]{"serverinfo"};
        this.category = category;
        this.help = "returns info about the current guild";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        EmbedBuilder eb = new EmbedBuilder();
        Guild g = event.getGuild();
        String id = g.getId();
        User owner = g.getOwner().getUser();
        List<Member> memberList = g.getMembers();
        int adminCount = 0;
        int memberCount = 0;
        int botCount = 0;
        for (Member m : memberList) {
            if (m.hasPermission(Permission.ADMINISTRATOR) && m.getUser().isBot()) {
                adminCount++;
            }
            if (m.getUser().isBot()) {
                botCount++;
            } else {
                memberCount++;
            }
        }

        Guild.VerificationLevel verificationLevel = g.getVerificationLevel();
        Guild.NotificationLevel notificationLevel = g.getDefaultNotificationLevel();

        List<Role> roles = g.getRoles();
        int roleCount = roles.size();
        StringBuilder sb = new StringBuilder();

        for (Role r : roles) {
            sb.append(r.getAsMention()).append(" ");
        }

        eb.setTitle(String.format("Guild info for: %s", g.getName()));
        eb.setImage(g.getIconUrl());
        Set<String> guildFeatures = g.getFeatures();
        StringBuilder descriptionBuilder = eb.getDescriptionBuilder();
        for (String feature : guildFeatures) {
            descriptionBuilder.append("`").append(feature).append("`\n");
        }


        eb.addField("ID", id, true);
        eb.addField("Owner", owner.getAsMention(), true);
        eb.addField("Admin count", String.valueOf(adminCount), true);
        eb.addField("User count", String.valueOf(memberCount), true);
        eb.addField("Bot count", String.valueOf(botCount), true);
        eb.addField("Role count", String.valueOf(roleCount), true);
        eb.addField("Verification level", verificationLevel.name().replaceAll("_", " ").toLowerCase(), true);
        eb.addField("Notification level", notificationLevel.name().replaceAll("_", " ").toLowerCase(), true);
        eb.addField("Features", guildFeatures.isEmpty() ? "None" : descriptionBuilder.toString(), false);
        event.reply(eb.build());

    }
}
