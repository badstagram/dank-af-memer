package badstagram.bots.dankafmemer.commands.info;

import badstagram.bots.dankafmemer.config.config;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.utils.MarkdownUtil;

public class botinfo extends Command {
    public botinfo(Category category) {
        this.name = "botinfo";
        this.help = "Displays information about the bot.";
        this.guildOnly = true;
        this.category = category;
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        EmbedBuilder eb = new EmbedBuilder();

        try {
            eb.setTitle("Bot info");
            eb.setThumbnail(event.getJDA().getSelfUser().getAvatarUrl());
            eb.addField("Developer", "<@424239181296959507>", true);
            eb.addField("Prefix", MarkdownUtil.monospace(event.getClient().getPrefix()), true);
            eb.addBlankField(false);
            eb.addField("JDA version", config.JDA_VERSION, true);
            eb.addField("JDA utilities version", config.JDA_UTILS_VERSION, true);
            eb.addField("Bot version", event.getSelfUser().getId().equals("639071511331995649") ? "DEV BUILD" : config.BOT_VERSION, true);

            event.reply(eb.build());
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), "botinfo");
        }

    }
}