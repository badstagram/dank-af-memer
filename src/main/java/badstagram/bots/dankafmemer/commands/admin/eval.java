package badstagram.bots.dankafmemer.commands.admin;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import groovy.lang.GroovyShell;
import net.dv8tion.jda.api.EmbedBuilder;


public class eval extends Command {

    private final GroovyShell engine;
    private final String imports;

    public eval(Category category) {
        this.name = "eval";
        this.aliases = new String[]{"ev"};
        this.ownerCommand = true;
        this.category = category;
        this.hidden = true;

        this.engine = new GroovyShell();
        this.imports = "import java.io.*\n" +
                "import java.lang.*\n" +
                "import java.util.*\n" +
                "import java.util.concurrent.*\n" +
                "import net.dv8tion.jda.api.*\n" +
                "import net.dv8tion.jda.api.entities.*\n" +
                "import net.dv8tion.jda.internal.entities.*;\n" +
                "import net.dv8tion.jda.api.managers.*\n" +
                "import net.dv8tion.jda.internal.managers.*;\n" +
                "import net.dv8tion.jda.api.utils.*\n" +
                "import badstagram.bots.dankafmemer.config.config\n";


    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        try {

            engine.setProperty("args", event.getArgs());
            engine.setProperty("event", event);
            engine.setProperty("message", event.getMessage());
            engine.setProperty("channel", event.getChannel());
            engine.setProperty("jda", event.getJDA());
            engine.setProperty("guild", event.getGuild());
            engine.setProperty("member", event.getMember());
            engine.setProperty("client", event.getClient());

            String script = imports + event.getMessage().getContentRaw().split("\\s+", 2)[1];
            Object out = engine.evaluate(script);

            event.getChannel().sendMessage(out == null ? "" : out.toString()).queue();
        } catch (Exception e) {
            event.getChannel().sendMessage(e.getMessage()).queue();
        }
    }
}

