package badstagram.bots.dankafmemer.commands.admin;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class flag extends Command {
    private Logger logger = LoggerFactory.getLogger(flag.class);

    public flag(Category category) {
        this.name = "flag";
        this.help = "Adds or removes a flag from a user";
        this.category = category;
        this.ownerCommand = true;
    }


    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }

        String[] args = event.getArgs().split(" \\| ");

        String id = args[0];
        String action = args[1];
        String flag = args[2];


        switch (action) {
            case "add":
                addFlag(id, flag);
            case "remove":
                removeFlag(id, flag);
        }
    }

    private void addFlag(String userId, String flag) {
        Connection connection = null;
        Statement statement1 = null;
        Statement statement2 = null;
        String selectSql, updateSql;
        ResultSet rs = null;
        List<String> flags;

        try {
            connection = DriverManager.getConnection(databaseUrls.USER_INFO_URL);
            statement1 = connection.createStatement();
            statement2 = connection.createStatement();
            selectSql = "SELECT flags FROM users WHERE user_id = " + userId;
            updateSql = "UPDATE users SET flags = '%s' WHERE user_id = %s";
            rs = statement1.executeQuery(selectSql);

            while (rs.next()) {
                flags = new ArrayList<>(Arrays.asList(rs.getString("flags").split(", ")));

                flags.add(flag);
                String finalFlags = String.join(", ", flags);
                String sql = String.format(updateSql, finalFlags, userId);
                //logger.debug(sql);
                statement2.execute(sql);


            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement1.close();
                statement2.close();
                rs.close();
            } catch (Exception ignored) {

            }
        }


    }


    private void removeFlag(String userId, String flag) {
        Connection connection = null;
        Statement statement1 = null;
        Statement statement2 = null;
        String selectSql, updateSql;
        ResultSet rs = null;
        List<String> flags;

        try {
            connection = DriverManager.getConnection(databaseUrls.USER_INFO_URL);
            statement1 = connection.createStatement();
            statement2 = connection.createStatement();
            selectSql = "SELECT flags FROM users WHERE user_id = " + userId;
            updateSql = "UPDATE users SET flags = '%s' WHERE user_id = %s";
            rs = statement1.executeQuery(selectSql);

            while (rs.next()) {
                flags = new ArrayList<>(Arrays.asList(rs.getString("flags").split(", ")));

                flags.remove(flag);
                String finalFlags = String.join(", ", flags);
                String sql = String.format(updateSql, finalFlags, userId);
                //logger.debug(sql);
                statement2.execute(sql);

                connection.close();
                statement1.close();
                statement2.close();
                rs.close();
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement1.close();
                statement2.close();
                rs.close();
            } catch (Exception ignored) {

            }
        }

    }
}

