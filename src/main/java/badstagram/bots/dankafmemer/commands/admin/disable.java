package badstagram.bots.dankafmemer.commands.admin;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class disable extends Command {
    public disable(Category category) {
        this.name = "disable";
        this.hidden = true;
        this.ownerCommand = true;
        this.category = category;
        this.help = "disables a command";

    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }

        utils.addUsage(getName(), event);
        String command = event.getArgs().trim().split("\\s+")[0];

        Connection connection;
        Statement statement;

        try {
            connection = DriverManager.getConnection(databaseUrls.COMMANDS_URL);
            statement = connection.createStatement();
            String sql = "UPDATE commandConfig SET enabled = 0 WHERE name = \"%s\"";
            String query = String.format(sql, command);
            statement.execute(query);
            event.reactSuccess();
            event.replyFormatted("Disabled %s", command);
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
            return;
        }
    }
}
