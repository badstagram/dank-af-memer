package badstagram.bots.dankafmemer.commands.admin;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

import java.awt.*;


public class ping extends Command {


    public ping(Category category) {
        this.name = "ping";
        this.category = category;
        this.help = "Gets the ping to discord";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        event.getJDA().getRestPing().queue((rest) -> {
            final long gatewayPing = event.getJDA().getGatewayPing();
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("Ping!");
            eb.setColor(Color.MAGENTA);
            eb.addField("Gateway", gatewayPing + "ms", true);
            eb.addField("Rest", rest + "ms", true);
            eb.setFooter(utils.getUptime(), null);
            event.reply(eb.build());
        });


    }
}
