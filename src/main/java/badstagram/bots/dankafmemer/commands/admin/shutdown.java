package badstagram.bots.dankafmemer.commands.admin;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class shutdown extends Command {
    public shutdown(Category category) {
        this.name = "shutdown";
        this.category = category;
        this.help = "Shuts down the bot";
        this.botPermissions = new Permission[]{Permission.MESSAGE_WRITE};
        this.guildOnly = true;
        this.hidden = true;
        this.ownerCommand = true;
        this.aliases = new String[]{"sd"};
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        event.reactSuccess();
        event.getTextChannel().sendMessage("Now shutting down! I was online for " + utils.getUptime()).queue();
        event.getJDA().shutdown();
        event.getJDA().getHttpClient().dispatcher().executorService().shutdownNow();
        event.getJDA().getHttpClient().connectionPool().evictAll();
    }
}
