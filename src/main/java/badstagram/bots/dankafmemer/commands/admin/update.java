package badstagram.bots.dankafmemer.commands.admin;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;

public class update extends Command {
    public update(Category category) {
        this.name = "update";
        this.ownerCommand = true;
        this.hidden = true;
        this.category = category;
        this.aliases = new String[]{"ud", "gitpull"};
        this.help = "Gets the latest update from github";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }

        utils.addUsage(getName(), event);
        Runtime runtime = Runtime.getRuntime();
        event.reply("Now updating the bot, please allow up to 3 minutes for the bot to fully come back online.");
        event.getJDA().shutdownNow();
        event.getJDA().getHttpClient().connectionPool().evictAll();

        try {
            boolean devMode = event.getArgs().split("\\s+")[0].equals("-d");
            runtime.exec("git pull origin development");
            runtime.exec("./gradlew shadowJar");
            runtime.exec(devMode ? "java -jar ./build/libs/dank-af-memer.jar -d" : "java -jar ./build/libs/dank-af-memer.jar -m");
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
