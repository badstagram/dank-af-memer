package badstagram.bots.dankafmemer.commands.admin;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class botmetrics extends Command {

    public botmetrics(Category category) {
        this.name = "botmetrics";
        this.ownerCommand = true;
        this.category = category;
        this.hidden = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        long numberOfCommands = event.getClient().getCommands().size();
        int numberOfUsers = 0;
        for (Guild g : event.getJDA().getGuildCache()) {
            for (Member ignored : g.getMembers()) {
                numberOfUsers += 1;
            }
        }

        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(event.getSelfUser().getAsTag());
        eb.addField("Number of commands", String.valueOf(event.getClient().getCommands().size()), true);
        eb.addField("Number of guilds", String.valueOf(numberOfCommands), true);
        eb.addField("Number of members in guilds", String.valueOf(numberOfUsers), true);
        eb.addField("Most used command", getMostUsedCommand(), true);

        event.reply(eb.build());

    }

    private String getMostUsedCommand() {

        Connection connection = null;
        Statement statement = null;
        String sql = "SELECT name, usages FROM commandConfig ORDER BY usages DESC LIMIT 1;";
        ResultSet rs = null;
        try {
            connection = DriverManager.getConnection(databaseUrls.COMMANDS_URL);
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);
            int usages = 0;
            String name = "";

            while (rs.next()) {
                usages = rs.getInt("usages");
                name = rs.getString("name");
            }
            connection.close();
            return String.format("`%s` : %s", name, usages);

        } catch (Exception e) {

            e.printStackTrace();


            return "";

        }
    }
}

