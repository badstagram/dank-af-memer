package badstagram.bots.dankafmemer.commands.steam;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

public class games extends Command {
    public games(Category category) {
        this.name = "games";
        this.help = "gets information about a steam user's games";
        this.arguments = "<SteamID64>";
        this.category = category;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        OkHttpClient client = new OkHttpClient();
        int games = 0;
        HttpUrl httpUrl;
        Request request;
        Response response;
        String id = event.getArgs().split("\\s+")[0];
        String jsonData;
        JSONObject json;

        try {

            httpUrl = new HttpUrl.Builder()
                    .scheme("https")
                    .host("api.steampowered.com")
                    .addPathSegments("IPlayerService/GetOwnedGames/v0001")
                    .addQueryParameter("key", utils.getEnvVar("steam_api_key"))
                    .addQueryParameter("steamid", id)
                    .addQueryParameter("include_appinfo", "1").build();

            request = new Request.Builder()
                    .url(httpUrl).build();

            response = client.newCall(request).execute();

            jsonData = response.body().string();

            JSONArray array = new JSONObject(jsonData).getJSONObject("response").getJSONArray("games");


            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle(String.format("Game info for: %s", utils.getSteamPersona(id, event)));
            StringBuilder descriptionBuilder = eb.getDescriptionBuilder();


            for (int i = 0; i < array.length(); i++) {
                json = array.getJSONObject(i);
                String name = json.getString("name");
                int playtime = json.getInt("playtime_forever");
                MessageEmbed.Field nameField = new MessageEmbed.Field("Game", name, true);
                MessageEmbed.Field playtimeField = new MessageEmbed.Field("playtime", String.valueOf(playtime / 60), true);

                eb.addField(nameField);
                eb.addField(playtimeField);
                eb.addBlankField(false);

            }

            String persona = utils.getSteamPersona(id, event);


            event.reply(eb.build());

        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
    }
}
