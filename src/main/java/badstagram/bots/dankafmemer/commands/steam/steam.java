package badstagram.bots.dankafmemer.commands.steam;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

public class steam extends Command {
    private static String persona;
    public steam(Category category) {
        this.name = "steam";
        this.help = "gets information about a steam user";
        this.arguments = "<SteamID64>";
        this.category = category;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        OkHttpClient client = new OkHttpClient();
        HttpUrl httpUrl;
        Request request;
        Response response;
        String id = event.getArgs().split("\\s+")[0];
        String jsonData;
        JSONObject json;
        try {
            httpUrl = new HttpUrl.Builder()
                    .scheme("https")
                    .host("api.steampowered.com")
                    .addPathSegments("ISteamUser/GetPlayerSummaries/v0002")
                    .addQueryParameter("key", utils.getEnvVar("steam_api_key"))
                    .addQueryParameter("steamids", id)
                    .build();

            request = new Request.Builder()
                    .url(httpUrl).build();

            response = client.newCall(request).execute();

            jsonData = response.body().string();
            json = new JSONObject(jsonData).getJSONObject("response").getJSONArray("players").getJSONObject(0);

            int communityVisibilityState = json.getInt("communityvisibilitystate");
            int profileState = json.getInt("profilestate");
            String profileUrl = json.getString("profileurl");
            String personaName = json.getString("personaname");
            String avatarUrl = json.getString("avatarfull");
            int personaState = json.getInt("personastate");
            String locCountryCode = json.getString("loccountrycode");

            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle(String.format("Steam info for: %s", personaName));
            //eb.addField("Profile state", getProfileState(profileState), true);

            if (profileState == 1) {
                eb.addField("Profile URL", MarkdownUtil.maskedLink("Here", profileUrl), true);
            } else {
                eb.addField("Profile URL", "Profile not set", true);
            }

            eb.addField("Online status", getPersonaState(personaState), true);
            eb.addField("Community visibility", getCommunityVisibilityState(communityVisibilityState), true);
            eb.addField("Country", locCountryCode, true);
            eb.setThumbnail(avatarUrl);

            event.reply(eb.build());

        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }

    }

    /**
     * Converts The communityVisibilityState from steam into a readable string.
     *
     * @param id The communityVisibilityState from steam.
     * @return The converted communityVisibilityState.
     */

    private String getCommunityVisibilityState(int id) {
        switch (id) {
            case 1:
                return "Private / Friends only";
            case 3:
                return "Public";
            default:
                return "\u2753";
        }
    }

    private String getProfileState(int id) {
        return id == 1 ? "Community profile set" : "Community profile not set";
    }

    private String getPersonaState(int id) {
        switch (id) {
            case 0:
                return "Offline";
            case 1:
                return "Online";
            case 2:
                return "Busy";
            case 3:
                return "Away";
            case 4:
                return "Snooze";
            case 5:
                return "Looking to trade";
            case 6:
                return "Looking to play";
            default:
                return "\u2753";
        }

    }

    static String getPersona() {
        return persona;
    }
}
