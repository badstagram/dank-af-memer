package badstagram.bots.dankafmemer.commands.image;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import okhttp3.OkHttpClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class birb extends Command {


    Logger logger = LoggerFactory.getLogger(birb.class);

    public birb(Category category) {
        this.name = "birb";
        this.category = category;
        this.help = "shows a random birb";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = true;
        this.aliases = new String[]{"bird"};
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        JSONObject jsonObject = null;

        jsonObject = utils.requestKsoftImage("birb", new OkHttpClient());


        String url = jsonObject.get("url").toString();

        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Birb");
        eb.setImage(url);
        eb.setFooter(String.format("Requested by: %s", event.getAuthor().getAsTag()));

        event.reply(eb.build());
    }
}
