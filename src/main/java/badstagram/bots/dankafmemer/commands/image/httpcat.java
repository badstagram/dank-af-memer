package badstagram.bots.dankafmemer.commands.image;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class httpcat extends Command {
    public httpcat(Category category) {
        this.name = "httpcat";
        this.help = "Gets a http cat";
        this.category = category;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        try {
            Logger logger = LoggerFactory.getLogger(httpcat.class);

            String code = event.getArgs().split("\\s+")[0];
            //
            String[] codes = new String[]{"100", "101", "200", "201", "202", "204", "206", "207", "300", "301", "302", "303", "304", "305", "307", "400", "401", "402", "403", "404", "405", "406", "408", "409", "410", "411", "402", "413", "414", "415", "416", "417", "418", "420", "421", "422", "423", "424", "425", "426", "429", "431", "444", "450", "451", "500", "501", "502", "503", "504", "506", "507", "508", "509", "510", "511", "599"};

            List<String> validCodes = new ArrayList<>(Arrays.asList(codes));

            if (!validCodes.contains(code)) {
                return;
            }

            EmbedBuilder eb = new EmbedBuilder();
            String url = String.format("https://http.cat/%s.jpg", code);
            eb.setTitle("Http cat");
            eb.setImage(url);
            event.reply(eb.build());


        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
    }
}
