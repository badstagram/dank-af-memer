package badstagram.bots.dankafmemer.commands.image;


import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.io.IOException;

public class meme extends Command {

    public meme(Category category) {
        this.name = "meme";
        this.category = category;
        this.help = "Sends a meme";
        this.aliases = new String[]{"dankmeme"};

    }

    @Override
    protected void execute(CommandEvent event) {

        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        OkHttpClient httpClient = new OkHttpClient();
        String title = null;
        String imageUrl = null;
        String subreddit = null;
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.ksoft.si")
                .addPathSegment("images")
                .addPathSegment("random-meme").build();

        Request request = new Request.Builder()
                .addHeader("Authorization", String.format("Bearer %s", utils.getEnvVar("ksoft")))
                .url(httpUrl)
                .build();

        try {
            Response response = httpClient.newCall(request).execute();

            String jsonData = response.body().string();
            JSONObject jsonObject = new JSONObject(jsonData);

            title = jsonObject.get("title").toString();
            imageUrl = jsonObject.get("image_url").toString();
            subreddit = jsonObject.get("subreddit").toString();

        } catch (IOException e) {
            event.reactError();
            e.printStackTrace();
        }

        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(title);
        eb.setImage(imageUrl);
        eb.setFooter("meme from " + subreddit, null);
        event.reply(eb.build());
    }
}