package badstagram.bots.dankafmemer.commands.image;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import okhttp3.OkHttpClient;
import org.json.JSONObject;

public class doge extends Command {
    public doge(Category category) {
        this.name = "doge";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.category = category;
        this.guildOnly = true;
        this.help = "Sends a doge image.";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        JSONObject jsonObject = null;
        String url = "";

        jsonObject = utils.requestKsoftImage("doge", new OkHttpClient());

        url = jsonObject.get("url").toString();

        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Doge");
        eb.setImage(url);
        eb.setFooter(String.format("Requested by: %s", event.getAuthor().getAsTag()));
        event.reply(eb.build());
    }
}
