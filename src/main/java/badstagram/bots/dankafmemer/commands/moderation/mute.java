package badstagram.bots.dankafmemer.commands.moderation;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class mute extends Command {
    public mute(Category category) {
        this.name = "mute";
        this.userPermissions = new Permission[]{Permission.MANAGE_ROLES};
        this.aliases = new String[]{"m"};
        this.category = category;
        this.arguments = "<@member> ' | ' [length] ' | ' [reason]";
        this.help = "Mutes a member.";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        Logger logger = LoggerFactory.getLogger(mute.class);
        String[] args = event.getArgs().split(" \\| ");
        String reason, length;
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();

        if (mentionedMembers.isEmpty()) {
            event.reply("You didn't mention a member to mute.");
            return;
        }
        length = args[1];
        reason = args[2];

        if (length.equals("")) {
            length = "perm";
        }

        if (reason.equals("")) {
            reason = "No reason provided";
        }

        Connection config = null;
        Connection punishments = null;
        Statement stmt = null;
        Role mutedRole = null;
        TextChannel punishLog = null;
        String mutedRoleId = "";
        String punishLogId = "";
        try {
            config = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
            punishments = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
            String selectSql = String.format("SELECT muted_role, punish_log FROM \"%s\"", event.getGuild().getId());
            stmt = config.createStatement();

            ResultSet rs = stmt.executeQuery(selectSql);

            while (rs.next()) {
                mutedRoleId = rs.getString("muted_role");
                punishLogId = rs.getString("punish_log");
            }
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
        if (mutedRoleId.equals("")) {
            event.reply("Muted role is not set. Set it using `%%config muted_role | <role id>`");
            return;
        }

        mutedRole = event.getGuild().getRoleById(mutedRoleId);

        if (punishLogId.equals("")) {
            event.reply("Punish log is not set. Set it using `%%config punish_log | <channel id>`");
            return;
        }
        punishLog = event.getGuild().getTextChannelById(punishLogId);

        TimeUnit timeUnit = null;

        if (length.equals("perm")) {
            event.getGuild().addRoleToMember(mentionedMembers.get(0), mutedRole).queue();
        }
        String lengthFormatted = "";

        if (length.endsWith("s")) {
            timeUnit = TimeUnit.SECONDS;
            lengthFormatted = length.replace("s", "");
        } else if (length.endsWith("m")) {
            timeUnit = TimeUnit.MINUTES;

            lengthFormatted = length.replace("m", "");
        } else if (length.endsWith("h")) {
            timeUnit = TimeUnit.HOURS;

            lengthFormatted = length.replace("h", "");
        } else if (length.endsWith("d")) {
            timeUnit = TimeUnit.DAYS;

            lengthFormatted = length.replace("d", "");
        }

        MessageEmbed punishLogEmbed = new EmbedBuilder()
                .setTitle(String.format("Mute"))
                .addField("User", mentionedMembers.get(0).getAsMention(), true)
                .addField("Moderator", event.getAuthor().getAsMention(), true)
                .addField("Length", length.equals("perm") ? "Permanent" : length, true)
                .addField("Reason", reason, true).build();

        MessageEmbed embedToUser = new EmbedBuilder()
                .setTitle("Mute")
                .addField("User", mentionedMembers.get(0).getAsMention(), true)
                .addField("Moderator", event.getAuthor().getAsMention(), true)
                .addField("Guild", event.getGuild().getName(), true)
                .addField("Length", length.equals("perm") ? "Permanent" : length, true)
                .addField("Reason", reason, true).build();


        event.getGuild().addRoleToMember(mentionedMembers.get(0), mutedRole).queue();


        try {
            punishments = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
            String sql = String.format("insert into \"%s\" (user_id, moderator_id, reason, punish_type) values (?,?,?,?);", event.getGuild().getId());
            PreparedStatement pstmt = punishments.prepareStatement(sql);
            pstmt.setString(1, mentionedMembers.get(0).getId());
            pstmt.setString(2, event.getAuthor().getId());
            pstmt.setString(3, reason);
            pstmt.setString(4, "mute");
            pstmt.execute();
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
        mentionedMembers.get(0).getUser().openPrivateChannel().queue((channel) -> {
            channel.sendMessage(embedToUser).queue(null, (throwable -> logger.warn(throwable.getMessage(), throwable)));
        });
        event.getGuild().removeRoleFromMember(mentionedMembers.get(0), mutedRole).queueAfter(Integer.parseInt(lengthFormatted), timeUnit);


    }
}
