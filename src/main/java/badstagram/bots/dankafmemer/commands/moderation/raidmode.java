package badstagram.bots.dankafmemer.commands.moderation;

import badstagram.bots.dankafmemer.config.config;
import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class raidmode extends Command {


    public raidmode(Category category) {
        this.name = "raidmode";
        this.help = "Sets the Guild verification to the highest possible, locks all channels and prevents users from joining.";
        this.aliases = new String[]{"lockdown"};
        this.arguments = "<enable|disable>";
        this.userPermissions = new Permission[]{Permission.MANAGE_SERVER};
        this.botPermissions = new Permission[]{Permission.MANAGE_SERVER};
        this.category = category;
        this.guildOnly = true;

    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        boolean raidmodeActive = false;
        Guild g = event.getGuild();
        String option = event.getArgs().split("\\s+")[0];


        switch (option) {
            case "enable":
                try {
                    connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
                    statement = connection.createStatement();
                    rs = statement.executeQuery(String.format("SELECT raid_mode FROM '%s'", event.getGuild().getId()));
                    while (rs.next()) {
                        raidmodeActive = rs.getBoolean("raid_mode");
                        if (raidmodeActive) {
                            event.reply("Raidmode is already active. Use `%%raidmode disable` to disable");

                            return;
                        } else {
                            enableRaidmode(g, statement, event.getMessage());
                            Logger logger = LoggerFactory.getLogger(raidmode.class);
                            logger.warn(String.format("Raidmode activated for %s (%s)", g.getName(), g.getId()));
                        }
                    }
                } catch (Exception e) {
                    return;
                }

            case "disable":
                try {
                    connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
                    statement = connection.createStatement();
                    rs = statement.executeQuery(String.format("SELECT raid_mode FROM '%s'", event.getGuild().getId()));
                    while (rs.next()) {
                        raidmodeActive = rs.getBoolean("raid_mode");
                        if (!raidmodeActive) {
                            event.reply("Raidmode is not active. Use `%%raidmode enable` to enable");
                            return;
                        } else {
                            disableRaidMode(g, statement, event.getMessage());
                            Logger logger = LoggerFactory.getLogger(raidmode.class);
                            logger.warn(String.format("Raidmode de-activated for %s (%s)", g.getName(), g.getId()));
                        }
                    }
                } catch (Exception e) {
                    return;
                }
        }
    }

    private static void enableRaidmode(@Nonnull Guild g, @Nonnull Statement statement, @Nonnull Message message) {

        try {
            String sql = "UPDATE " + "'" + g.getId() + "'" + " SET 'raid_mode' = 1";
            statement.executeUpdate(sql);
            config.oldVerificationLevel = g.getVerificationLevel();
            g.getManager().setVerificationLevel(Guild.VerificationLevel.HIGH).queue();

            for (TextChannel tc : g.getTextChannels()) {
                config.oldPermissions.put(tc.getIdLong(), tc.getPermissionOverride(g.getPublicRole()));
                tc.getManager().putPermissionOverride(g.getPublicRole(), Permission.getRaw(Permission.MESSAGE_READ), Permission.getRaw(Permission.MESSAGE_WRITE)).queue();
            }
        } catch (Exception e) {
            utils.error(e, message, g, "raidmode enable");
        }
    }

    private static void disableRaidMode(@Nonnull Guild g, @Nonnull Statement statement, @Nonnull Message message) {
        String selectSql = "SELECT 'raid_mode' FROM " + g.getId();
        String updateSql = "update " + "'" + g.getId() + "'" + "set 'raid_mode' = 0";
        boolean raidmodeActive;
        try {
            ResultSet rs = statement.executeQuery(selectSql);
            while (rs.next()) {
                raidmodeActive = rs.getBoolean("raid_mode");
                if (!raidmodeActive) {
                    statement.executeUpdate(updateSql);

                    g.getManager().setVerificationLevel(config.oldVerificationLevel).queue();
                    for (TextChannel tc : g.getTextChannels()) {
                        long id = tc.getIdLong();


                        try {
                            tc.getManager().sync().queue();
                        } catch (IllegalArgumentException e) {
                            return;
                        }
                    }
                    return;
                }
            }
        } catch (Exception e) {
            utils.error(e, message, g, "raidmode disable");
        }
    }
}
