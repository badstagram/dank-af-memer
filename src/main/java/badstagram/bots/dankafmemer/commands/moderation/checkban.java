package badstagram.bots.dankafmemer.commands.moderation;

import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.List;

public class checkban extends Command {
    public checkban(Category category) {
        this.name = "checkban";
        this.category = category;
        this.help = "checks if a user is banned on ksoft.si";
        this.aliases = new String[]{"cban"};
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        Logger logger = LoggerFactory.getLogger(checkban.class);
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();

        if (mentionedMembers.isEmpty()) {
            event.reply("You need to mention a member to get ban info.");
            return;
        } else {
            OkHttpClient httpClient = new OkHttpClient();

            long userID = mentionedMembers.get(0).getIdLong();
            String name = null;
            String reason = null;
            String discriminator = null;
            String proof = null;
            boolean exists = false;
            boolean is_ban_active = false;


            HttpUrl httpUrl = new HttpUrl.Builder()
                    .scheme("https")
                    .host("api.ksoft.si")
                    .addPathSegment("bans")
                    .addPathSegment("info")
                    .addQueryParameter("user", String.valueOf(userID))
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Authorization", String.format("Bearer %s", utils.getEnvVar("ksoft")))

                    .url(httpUrl)
                    .build();


            try {

                Response response = httpClient.newCall(request).execute();

                String jsonData = response.body().string();
                logger.debug(jsonData);
                JSONObject jsonObject = new JSONObject(jsonData);
                exists = jsonObject.getBoolean("exists");

                if (exists) {
                    proof = jsonObject.getString("proof");
                    reason = jsonObject.getString("reason");
                    is_ban_active = jsonObject.getBoolean("is_ban_active");
                } else {
                    return;
                }

            } catch (Exception e) {
                event.reactError();
                e.printStackTrace();
            }


            EmbedBuilder eb = new EmbedBuilder();


            if (exists) {
                eb.setTitle("Ban info");
                eb.setColor(new Color(255, 0, 0));
                eb.addField("Reason", reason, true);
                eb.addField("Ban active", is_ban_active ? "No" : "Yes", true);
                eb.addField("Proof", proof, true);
                event.reply(eb.build());
            } else {
                event.reply("No ban found for that user.");
            }
        }
    }
}
