package badstagram.bots.dankafmemer.commands.moderation;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.concurrent.TimeUnit;

public class Guildconfig extends Command {
    private String muted_role, punish_log, mod_log, appeal_site;
    private boolean anti_advertise, raid_mode;
    private Logger logger = LoggerFactory.getLogger(Guildconfig.class);

    public Guildconfig(Category category) {
        this.name = "configure";
        this.aliases = new String[]{"config"};
        this.userPermissions = new Permission[]{Permission.MANAGE_SERVER};
        this.category = category;
        this.arguments = "<setting> | <value>";
        this.help = "Sets the config for the guild.";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        try {


            final String guildId = event.getGuild().getId();
            final String content = event.getMessage().getContentRaw();
            final String prefix = event.getClient().getPrefix();
            if (content.equals(String.format("%sconfig", prefix)) || content.equals(String.format("%sconfigure", prefix))) {

                getConfigSettings(guildId, event);
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle(String.format("Config for %s", event.getGuild().getName()));
                eb.addField("Muted role", "<@&" + muted_role + ">", true);
                eb.addField("Punish log", "<#" + punish_log + ">", true);
                eb.addField("Mod log", "<#" + mod_log + ">", true);
                eb.addField("Anti advertise", anti_advertise ? "Enabled" : "Disabled", true);
                eb.addField("Raid mode", raid_mode ? "Enabled" : "Disabled", true);
                eb.addField("Appeal site", appeal_site, true);
                eb.setFooter("Use %%config <setting> | <value> to set");
                event.reply(eb.build());
                return;
            }

            final String[] args = event.getArgs().split(" \\| ");
            final String setting = args[0].replaceAll("-", "_");
            final String value = args[1].replaceAll("-", "_");

            if (setting.equalsIgnoreCase("raidmode")) {
                return;
            }


            String sql = getSqlQuery(setting, guildId);
            if (sql.equals("No")) {
                event.reply("Invalid syntax");
                return;
            }

            Connection connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            //preparedStatement.setString(1, setting);
            preparedStatement.setString(1, value);
            preparedStatement.execute();
            Logger logger = LoggerFactory.getLogger(Guildconfig.class);
            logger.info(String.format("Config for guild %s (%s) updated by %s (%s), %s = %s", event.getGuild().getName(), guildId, event.getAuthor().getAsTag(), event.getAuthor().getId(), setting, value));
            event.getChannel().sendMessage("Config updated!").queue((message) -> message.delete().queueAfter(1, TimeUnit.MINUTES));


        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }

    }

    void getConfigSettings(String guildId, CommandEvent event) {

        try {
            Connection connection;
            Statement statement;
            ResultSet rs;

            connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);

            String sql = String.format("SELECT * FROM '%s'", guildId);

            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            while (rs.next()) {
                muted_role = rs.getString("muted_role");
                punish_log = rs.getString("punish_log");
                mod_log = rs.getString("mod_log");
                anti_advertise = rs.getBoolean("anti_advertise");
                raid_mode = rs.getBoolean("raid_mode");
                appeal_site = rs.getString("appeal_site");
            }
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
    }

    /**
     * @param configSetting The setting to modify.
     * @param guildId       The id for the guild.
     * @return A generated SQL query
     */
    String getSqlQuery(String configSetting, String guildId) {
        switch (configSetting) {
            case "muted_role":
                return "UPDATE '" + guildId + "' SET muted_role = ? WHERE id = 1";
            case "punish_log":
                return "UPDATE '" + guildId + "' SET punish_log = ? WHERE id = 1";
            case "mod_log":
                return "UPDATE '" + guildId + "' SET mod_log = ? WHERE id = 1";
            case "anti_advertise":
                return "UPDATE '" + guildId + "' SET anti_advertise = ? WHERE id = 1";
            case "appeal_site":
                return "UPDATE '" + guildId + "' SET appeal_site = ? WHERE id = 1";

            default:
                return "No";
        }
    }
}
