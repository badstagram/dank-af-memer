package badstagram.bots.dankafmemer.commands.moderation;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

public class ban extends Command {
    Logger logger = LoggerFactory.getLogger(ban.class);

    public ban(Category category) {
        this.name = "ban";
        this.category = category;
        this.help = "Bans a member from the guild.";
        this.botPermissions = new Permission[]{
                Permission.MESSAGE_EMBED_LINKS,
                Permission.BAN_MEMBERS
        };
        this.userPermissions = new Permission[]{Permission.BAN_MEMBERS};
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);

        Connection connection = null;
        PreparedStatement pstmt = null;
        String punishLogId = "";
        String appealSite = "";

        Message message = event.getMessage();
        List<Member> mentionedMembers = message.getMentionedMembers();

        if (mentionedMembers.isEmpty()) {
            event.reply("You didn't mention a member to ban.");
            return;
        }

        Member target = mentionedMembers.get(0);
        Member moderator = event.getMember();
        String reason = event.getArgs().split(" \\| ")[1];
        try {
            connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
            String sql = String.format("SELECT punish_log, appeal_site FROM \"%s\"", event.getGuild().getId());
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                punishLogId = rs.getString("punish_log");
                appealSite = rs.getString("appeal_site");
            }
        } catch (SQLException e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }

        TextChannel punishLog = event.getGuild().getTextChannelById(punishLogId);
        EmbedBuilder embedToUser = new EmbedBuilder();
        embedToUser.setTitle("Ban");
        embedToUser.addField("User", target.getAsMention(), true);
        embedToUser.addField("Moderator", moderator.getAsMention(), true);
        embedToUser.addField("Guild", event.getGuild().getName(), true);
        embedToUser.addField("Reason", reason, true);
        if (!appealSite.equals("not set")) {
            embedToUser.setFooter(String.format("Appeal at %s", appealSite));
        }


        MessageEmbed punishLogEmbed = new EmbedBuilder()
                .setTitle("Ban")
                .addField("User", target.getAsMention(), true)
                .addField("Moderator", moderator.getAsMention(), true)
                .addField("Reason", reason, true).build();

        if (punishLog != null) {
            punishLog.sendMessage(punishLogEmbed).queue();
        }

        target.getUser().openPrivateChannel().complete().sendMessage(embedToUser.build()).complete();
        event.getGuild().ban(target, 7).reason(String.format("Banned by %s (%s) for %s",
                moderator.getUser().getAsTag(),
                moderator.getUser().getId(),
                reason
        )).queue();


        try {
            connection = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
            String sql = String.format("INSERT INTO \"%s\" (user_id, moderator_id, reason, punish_type) VALUES (?,?,?,?)", event.getGuild().getId());
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, target.getUser().getId());
            pstmt.setString(2, moderator.getUser().getId());
            pstmt.setString(3, reason);
            pstmt.setString(4, "ban");

            pstmt.execute();
        } catch (SQLException e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
    }
}