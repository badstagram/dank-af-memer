package badstagram.bots.dankafmemer.commands.moderation;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.checks;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

public class kick extends Command {
    Logger logger = LoggerFactory.getLogger(kick.class);

    public kick(Category category) {
        this.name = "kick";
        this.category = category;
        this.help = "Kicks a member from the guild.";
        this.botPermissions = new Permission[]{
                Permission.MESSAGE_EMBED_LINKS,
                Permission.KICK_MEMBERS
        };
        this.userPermissions = new Permission[]{Permission.KICK_MEMBERS};

    }

    @Override
    protected void execute(CommandEvent event) {
        if (!checks.commandDisabled(getName(), event)) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error while running that command. Check the syntax and try again.");
            eb.setDescription("`[423] This command is currently disabled.`");
            event.reply(eb.build());
            return;
        }
        utils.addUsage(getName(), event);
        Connection connection = null;
        PreparedStatement pstmt = null;
        String punishLogId = "";

        Message message = event.getMessage();
        List<Member> mentionedMembers = message.getMentionedMembers();

        if (mentionedMembers.isEmpty()) {
            event.reply("You didn't mention a member to kick.");
            return;
        }

        Member target = mentionedMembers.get(0);
        Member moderator = event.getMember();
        String reason = event.getArgs().split(" \\| ")[1];
        try {
            connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
            String sql = String.format("SELECT punish_log FROM \"%s\"", event.getGuild().getId());
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                punishLogId = rs.getString("punish_log");
            }
        } catch (SQLException e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }

        TextChannel punishLog = event.getGuild().getTextChannelById(punishLogId);
        MessageEmbed embedToUser = new EmbedBuilder()
                .setTitle("Kick")
                .addField("User", target.getAsMention(), true)
                .addField("Moderator", moderator.getAsMention(), true)
                .addField("Guild", event.getGuild().getName(), true)
                .addField("Reason", reason, true).build();

        target.getUser().openPrivateChannel().queue((channel) -> {
            channel.sendMessage(embedToUser).queue(null, ((err) -> {
                logger.warn(String.format("Unable to send message to: %s (%s): %s", target.getUser().getAsTag(), target.getUser().getId(), err.getMessage()));
            }));
        });

        MessageEmbed punishLogEmbed = new EmbedBuilder()
                .setTitle(String.format("Kick"))
                .addField("User", target.getAsMention(), true)
                .addField("Moderator", moderator.getAsMention(), true)
                .addField("Reason", reason, true).build();

        if (punishLog != null) {
            punishLog.sendMessage(punishLogEmbed).queue();
        }
        event.getGuild().kick(target).reason(reason).queue();
        try {
            connection = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
            String sql = String.format("INSERT INTO \"%s\" (user_id, moderator_id, reason, punish_type) VALUES (?,?,?,?)", event.getGuild().getId());
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, target.getUser().getId());
            pstmt.setString(2, moderator.getUser().getId());
            pstmt.setString(3, reason);
            pstmt.setString(4, "ban");

            pstmt.execute();
        } catch (SQLException e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }
    }
}