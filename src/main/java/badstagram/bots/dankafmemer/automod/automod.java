package badstagram.bots.dankafmemer.automod;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import javax.annotation.Nonnull;
import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


public class automod {


    public static void antiAdvertise(@Nonnull Message message, @Nonnull Guild g) {
        boolean antiAdvertiseActive = false;
        String punishlog = "";
        AtomicReference<Connection> punishments = new AtomicReference<>();
        String sql = String.format("insert into \"%s\" (user_id, moderator_id, reason, punish_type) values (%s, '597877235017318407', '[automod] Advertising', 'softban');", g.getId(), message.getAuthor().getId());
        try {

            Connection guildConfig = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
            punishments.set(DriverManager.getConnection(databaseUrls.PUNISHMENT_URL));
            Statement guildConfigStatement = guildConfig.createStatement();

            ResultSet rs = guildConfigStatement.executeQuery(String.format("SELECT * FROM '%s'", g.getId()));
            while (rs.next()) {
                antiAdvertiseActive = rs.getBoolean("anti_advertise");
                punishlog = rs.getString("punish_log");
            }

        } catch (Exception e) {
            utils.error(e, message, g, "automod.antiAdvertise");
        }

        if (antiAdvertiseActive) {
            List<String> invites = message.getInvites();

            if (!invites.isEmpty()) {
                long userId = message.getIdLong();

                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("Punishment | Softban");
                eb.addField("Guild", g.getName(), true);
                eb.addField("Moderator", g.getJDA().getSelfUser().getAsMention(), true);
                eb.addField("Reason", "[Automod] advertising", true);
                utils.dmUser(message.getAuthor(), eb.build());

                EmbedBuilder embedToUser = new EmbedBuilder();
                EmbedBuilder embedForPunishlog = new EmbedBuilder();


                embedToUser.setColor(new Color(255, 59, 0));
                embedToUser.setTitle(String.format("Punishment | %s | Softban", message.getAuthor().getAsTag()), message.getAuthor().getAvatarUrl());
                embedToUser.addField("Moderator", message.getJDA().getSelfUser().getAsMention(), true);
                embedToUser.addField("Guild", message.getGuild().getName(), true);
                embedToUser.addField("User", message.getAuthor().getAsMention(), true);
                embedToUser.addField("Reason", "[Automod] Advertising", true);

                embedForPunishlog.setColor(new Color(255, 59, 0));
                embedForPunishlog.setTitle(String.format("Punishment | %s | Softban", message.getAuthor().getAsTag()), message.getAuthor().getAvatarUrl());
                embedForPunishlog.addField("Moderator", message.getJDA().getSelfUser().getAsMention(), true);
                embedForPunishlog.addField("User", message.getAuthor().getAsMention(), true);
                embedForPunishlog.addField("Reason", "[Automod] Advertising", true);


                try {
                    String userid = message.getAuthor().getId();
                    message.getGuild().ban(message.getAuthor(), 1).reason("[Automod] Advertising").queue((l) -> {
                        utils.dmUser(message.getAuthor(), embedToUser.build());
                        Statement stmt = null;
                        try {
                            stmt = punishments.get().createStatement();
                            stmt.execute(sql);
                        } catch (Exception e) {
                            utils.error(e, message, g, "automod.antiadvertise");
                        }
                    });
                    message.getGuild().getTextChannelById(punishlog).sendMessage(embedForPunishlog.build()).queue();
                    message.getGuild().unban(userid).queue();
                } catch (Exception e) {
                    utils.error(e, message, g, "automod.antiadvertise");
                }
            }

        }
    }

    public static void massMention(@Nonnull Message message) {
        List<User> mentions = message.getMentionedUsers();
        String userid = message.getAuthor().getId();
        if (mentions.size() > 4) {
            message.delete().reason(String.format("[automod] mass mention (%s)", mentions.size())).queue();

        }
    }

}
