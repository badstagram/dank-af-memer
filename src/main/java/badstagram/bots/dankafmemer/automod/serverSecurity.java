package badstagram.bots.dankafmemer.automod;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.utils.utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

public class serverSecurity {
    public static void userBot(Member m, Message msg, Guild g, GuildMessageReceivedEvent event) {
        Logger logger = LoggerFactory.getLogger(serverSecurity.class);
        Connection connection = null;
        PreparedStatement pstmt = null;
        String punishLogId = "";

        try {
            connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
            String sql = String.format("SELECT punish_log FROM \"%s\"", g.getId());
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                punishLogId = rs.getString("punish_log");
            }
        } catch (Exception e) {
            utils.error(e, msg, g, "serversecurity.userbot");
            return;
        }

        TextChannel punishLog = null;
        if (punishLogId != null) {
            punishLog = event.getGuild().getTextChannelById(punishLogId);
        }
        MessageEmbed embedToUser = new EmbedBuilder()
                .setTitle("Kick")
                .addField("User", m.getAsMention(), true)
                .addField("Moderator", m.getJDA().getSelfUser().getId(), true)
                .addField("Guild", event.getGuild().getName(), true)
                .addField("Reason", "[Server Security] User Bot", true).build();

        MessageEmbed punishLogEmbed = new EmbedBuilder()
                .setTitle("Kick")
                .addField("User", m.getAsMention(), true)
                .addField("Moderator", m.getJDA().getSelfUser().getAsMention(), true)
                .addField("Reason", "[Server Security] User Bot", true).build();

        punishLog.sendMessage(punishLogEmbed).queue();
        List<Activity> activities = m.getActivities();
        for (Activity activity : activities) {
            if (activity.getType().equals(Activity.ActivityType.WATCHING)) {
                try {
                    connection = DriverManager.getConnection(databaseUrls.GUILD_CONFIG_URL);
                    String sql = String.format("INSERT INTO %s (user_id, moderator_id, reason, punish_type) VALUES (?,?,?,?)", g.getId());
                    pstmt = connection.prepareStatement(sql);
                    pstmt.setString(1, g.getId());
                    pstmt.setString(2, m.getId());
                    pstmt.setString(3, m.getJDA().getSelfUser().getId());
                    pstmt.setString(4, "[Server Security] User Bot]");
                    pstmt.setString(5, "kick");


                } catch (Exception e) {
                    utils.error(e, msg, g, "serversecurity.userbot");
                    return;
                }
                m.getUser().openPrivateChannel().queue((channel) -> {
                    channel.sendMessage(embedToUser).queue(null, ((err) -> {
                        logger.warn(String.format("Unable to send message to: %s (%s): %s", m.getUser().getAsTag(), m.getUser().getId(), err.getMessage()));
                        return;
                    }));
                });
                g.kick(m).reason("[Server Security] User Bot").queue();

            }
        }
    }
}


