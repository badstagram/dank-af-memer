package badstagram.bots.dankafmemer.utils;

import badstagram.bots.dankafmemer.config.databaseUrls;
import badstagram.bots.dankafmemer.logging.log;
import com.jagrosh.jdautilities.command.CommandEvent;
import io.github.cdimascio.dotenv.Dotenv;
import io.sentry.Sentry;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.awt.*;
import java.lang.management.ManagementFactory;
import java.sql.*;

public class utils {
    static Logger logger = LoggerFactory.getLogger(utils.class);

    public static JSONObject requestKsoftImage(@Nonnull String tag, @Nonnull OkHttpClient client) {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.ksoft.si")
                .addPathSegment("images")
                .addPathSegment("random-image")
                .addQueryParameter("tag", tag).build();

        Request request = new Request.Builder()
                .addHeader("Authorization", String.format("Bearer %s", utils.getEnvVar("ksoft")))
                .url(httpUrl)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (Exception e) {
            logger.error(e.getMessage());
            for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                logger.error(stackTraceElement.toString());
            }

        }
        String jsonData = "";
        try {
            jsonData = response.body().string();
        } catch (Exception e) {
            logger.error(e.getMessage());
            for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                logger.error(stackTraceElement.toString());
            }
            logErrorToSentry(e);
        }


        return new JSONObject(jsonData);
    }

    public static void displayFatalErrorMessage(@Nonnull EmbedBuilder eb, @Nonnull String message, @Nonnull TextChannel channel, @Nonnull CommandEvent event) {
        eb.setTitle("Fatal error");
        eb.setDescription(String.format("This should be reported to my developer! " +
                "```%s```", message));
        eb.setFooter(String.format("Join the discord to report this error: %s", event.getClient().getServerInvite()));

        channel.sendMessage(eb.build()).queue();
    }


    public static void logErrorToSentry(@Nonnull Exception e) {

        Sentry.init(utils.getEnvVar("sentry_dsn"));
        Sentry.capture(e);
    }


    public static String getUptime() {
        final long duration = ManagementFactory.getRuntimeMXBean().getUptime();

        final long years = duration / 31104000000L;
        final long months = duration / 2592000000L % 12;
        final long days = duration / 86400000L % 30;
        final long hours = duration / 3600000L % 24;
        final long minutes = duration / 60000L % 60;
        final long seconds = duration / 1000L % 60;
        // final long milliseconds = duration % 1000;

        String uptime = (years == 0 ? "" : years + " Years, ") + (months == 0 ? "" : months + " Months, ") + (days == 0 ? "" : days + " days, ") + (hours == 0 ? "" : hours + " Hours, ")
                + (minutes == 0 ? "" : minutes + " minutes, ") + (seconds == 0 ? "" : seconds + " Seconds, ") /* + (milliseconds == 0 ? "" : milliseconds + " Milliseconds, ") */;

        uptime = replaceLast(uptime, ", ", "");
        uptime = replaceLast(uptime, ",", " and");

        return uptime;
    }

    private static String replaceLast(final String text, final String regex, final String replacement) {
        return text.replaceFirst("(?s)(.*)" + regex, "$1" + replacement);
    }

    public static String getEnvVar(@Nonnull String key) {
        return Dotenv.load().get(key.toUpperCase());
    }

    public static void dmUser(User u, String message) {
        try {

            u.openPrivateChannel().queue((privateChannel -> {
                privateChannel.sendMessage(message).queue(null, (throwable) -> {

                    log.WARN(logger, throwable);
                });
            }));
        } catch (Exception e) {

            log.ERROR(logger, e);
        }
    }

    public static void dmUser(User u, MessageEmbed embed) {
        try {

            u.openPrivateChannel().queue((privateChannel -> {
                privateChannel.sendMessage(embed).queue(null, (throwable) -> {
                    log.WARN(logger, throwable);

                });
            }));
        } catch (Exception e) {
            log.ERROR(logger, e);
        }
    }

    public static void error(@Nonnull Exception e, @Nonnull Message message, @Nonnull Guild g, @Nonnull String command) {
        utils.logErrorToSentry(e);

        EmbedBuilder embedToUser = new EmbedBuilder();
        EmbedBuilder embedToDev = new EmbedBuilder();

        embedToUser.setTitle("There was an error while running that command. Check the syntax and try again");
        embedToUser.setColor(new Color(255, 0, 0));
        embedToUser.setDescription(String.format("`[500] %s`", e.getMessage()));
        message.getChannel().sendMessage(embedToUser.build()).queue();

        embedToDev.setTitle("Hi, something broke, pls fix kthx");
        embedToDev.addField("Exception", e.getMessage(), false);
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement ste : e.getStackTrace()) {
            sb.append(ste.toString());
            sb.append("\n");
        }
        embedToDev.addField("Stack Trace", sb.toString().substring(1, 1024), false);
        embedToDev.addField("Command", MarkdownUtil.monospace(command), false);
        embedToDev.setFooter(String.format("Guild: %s (%s)", g.getName(), g.getId()), g.getIconUrl());
        embedToDev.setColor(new Color(255, 0, 0));
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.setEmbed(embedToDev.build());
        messageBuilder.append("<@424239181296959507>");
        g.getJDA().getGuildById(628213929624993792L).getTextChannelById(633827817938747403L).sendMessage(messageBuilder.build()).queue();
    }


    public static int getBans(Guild g, User u) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        int amount = 0;

        connection = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
        String sql = String.format("SELECT * FROM \"%s\" WHERE punish_type = 'ban' AND user_id = %s", g.getId(), u.getId());
        stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            amount++;
        }
        return amount;

    }

    public static int getKicks(Guild g, User u) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        int amount = 0;

        connection = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
        String sql = String.format("SELECT * FROM \"%s\" WHERE punish_type = 'kick' AND user_id = %s", g.getId(), u.getId());
        stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            amount++;
        }
        return amount;

    }

    public static int getMutes(Guild g, User u) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        int amount = 0;

        connection = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
        String sql = String.format("SELECT * FROM \"%s\" WHERE punish_type = 'mute' AND user_id = %s", g.getId(), u.getId());
        stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            amount++;
        }
        return amount;

    }

    public static int getWarns(Guild g, User u) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        int amount = 0;

        connection = DriverManager.getConnection(databaseUrls.PUNISHMENT_URL);
        String sql = String.format("SELECT * FROM \"%s\" WHERE punish_type = 'warn' AND user_id = %s", g.getId(), u.getId());
        stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            amount++;
        }
        return amount;

    }

    public static String getSteamPersona(String steamID64, CommandEvent event) {
        OkHttpClient client = new OkHttpClient();
        HttpUrl httpUrl;
        Request request;
        Response response;

        String jsonData;
        JSONObject json;
        try {
            httpUrl = new HttpUrl.Builder()
                    .scheme("https")
                    .host("api.steampowered.com")
                    .addPathSegments("ISteamUser/GetPlayerSummaries/v0002")
                    .addQueryParameter("key", utils.getEnvVar("steam_api_key"))
                    .addQueryParameter("steamids", steamID64)
                    .build();

            request = new Request.Builder()
                    .url(httpUrl).build();

            response = client.newCall(request).execute();

            jsonData = response.body().string();
            json = new JSONObject(jsonData).getJSONObject("response").getJSONArray("players").getJSONObject(0);


            return json.getString("personaname");


        } catch (Exception e) {
            error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        }

        return steamID64;
    }

    public static void addUsage(String commandName, CommandEvent event) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = DriverManager.getConnection(databaseUrls.COMMANDS_URL);
            statement = connection.prepareStatement("UPDATE commandConfig SET usages = usages + 1 WHERE name = ?");
            statement.setString(1, commandName);
            statement.execute();


        } catch (Exception e) {
            error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        } finally {
            try {
                connection.close();
                statement.close();
                rs.close();
            } catch (Exception e) {
                error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
            }
        }

    }
}
