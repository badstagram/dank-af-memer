package badstagram.bots.dankafmemer.utils;

import badstagram.bots.dankafmemer.config.databaseUrls;
import com.jagrosh.jdautilities.command.CommandEvent;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class checks {

    /**
     * checks if a command is disabled.
     * @param command The command to check.
     * @param event   The CommandEvent that invoked the disable
     * @return @code{true} If the command is disabled or @code{false} if the command is enabled
     */

    public static boolean commandDisabled(@Nonnull String command, @Nonnull CommandEvent event) {

        Connection connection = null;
        Statement statement = null;
        boolean enabled = true;
        ResultSet rs = null;

        String sql = "SELECT enabled FROM commandConfig WHERE name = \"%s\" ";
        String query = String.format(sql, command);

        try {
            connection = DriverManager.getConnection(databaseUrls.COMMANDS_URL);
            statement = connection.createStatement();
            rs = statement.executeQuery(query);
            while (rs.next()) {
                enabled = rs.getBoolean("enabled");
            }
        } catch (Exception e) {
            utils.error(e, event.getMessage(), event.getGuild(), event.getMessage().getContentRaw());
        } finally {

            try {
                connection.close(); // <-- This is important
                statement.close();
                rs.close();
            } catch (Exception ignored) {

            }
        }

        return enabled;
    }
}