package badstagram.bots.dankafmemer.logging;


import org.slf4j.Logger;

import javax.annotation.Nonnull;

public class log {

    public static void WARN(@Nonnull Logger logger, @Nonnull Throwable throwable) {
        logger.warn(throwable.getMessage());
    }

    public static void WARN(@Nonnull Logger logger, @Nonnull String message) {
        logger.warn(message);
    }

    public static void ERROR(@Nonnull Logger logger, @Nonnull Throwable throwable) {
        logger.error(throwable.getMessage());
    }

    public static void ERROR(@Nonnull Logger logger, @Nonnull String message) {
        logger.error(message);
    }

    public static void ERROR(@Nonnull Logger logger, @Nonnull Exception ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
    }
}
