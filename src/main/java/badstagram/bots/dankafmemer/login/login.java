package badstagram.bots.dankafmemer.login;

import badstagram.bots.dankafmemer.commands.admin.*;
import badstagram.bots.dankafmemer.commands.fun.snipe;
import badstagram.bots.dankafmemer.commands.image.*;
import badstagram.bots.dankafmemer.commands.info.*;
import badstagram.bots.dankafmemer.commands.moderation.*;
import badstagram.bots.dankafmemer.commands.steam.*;
import badstagram.bots.dankafmemer.config.IDs;
import badstagram.bots.dankafmemer.config.config;
import badstagram.bots.dankafmemer.config.constants;
import badstagram.bots.dankafmemer.listeners.messageListener;
import badstagram.bots.dankafmemer.listeners.onGuildAdd;
import badstagram.bots.dankafmemer.listeners.onReady;
import badstagram.bots.dankafmemer.listeners.userJoinListener;
import badstagram.bots.dankafmemer.utils.utils;
import com.jagrosh.jdautilities.command.Command.Category;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;


public class login {
    private static Category imageCategory = new Category("image");
    private static Category adminCategory = new Category("admin");
    private static Category funCategory = new Category("fun");
    private static Category moderationCategory = new Category("moderation");
    private static Category infoCategory = new Category("info");
    private static Category steamCategory = new Category("steam");

    public static void startBot(EventWaiter waiter, String arg) throws Exception {
        CommandClientBuilder client = new CommandClientBuilder();
        client.setOwnerId(String.valueOf(IDs.OWNER_ID));
        client.setEmojis(constants.SUCCESS, constants.WARNING, constants.ERROR);
        client.setPrefix(arg.equals("-d") ? "!!" : "%%");
        client.setServerInvite("https://discord.gg/EZe7kN5");
        //client.useHelpBuilder(true);

        client.addCommands(
                //image
                new cat(imageCategory),
                new dog(imageCategory),
                new birb(imageCategory),
                new doge(imageCategory),
                new meme(imageCategory),
                new httpcat(imageCategory),

                //admin
                new ping(adminCategory),
                new shutdown(adminCategory),
                new eval(adminCategory),
                new update(adminCategory),
                new enable(adminCategory),
                new disable(adminCategory),
                new flag(adminCategory),
                new botmetrics(adminCategory),

                //moderation

                new checkban(moderationCategory),
                new kick(moderationCategory),
                new mute(moderationCategory),
                new Guildconfig(moderationCategory),
                new raidmode(moderationCategory),

                //fun
                new snipe(funCategory),

                //info
                new botinfo(infoCategory),
                new userinfo(infoCategory),
                new guildInfo(infoCategory),
                new roleInfo(infoCategory),
                new discordrep(infoCategory),

                //steam
                new steam(steamCategory),
                new games(steamCategory)
        );

        JDA jda = new JDABuilder(arg.equals("-d") ? utils.getEnvVar("dev_token") : utils.getEnvVar("main_token"))
                .addEventListeners(waiter, client.build(), new messageListener(), new onGuildAdd(), new onReady(), new userJoinListener())
                .setActivity(Activity.playing("Version: " + config.BOT_VERSION))
                .build();


    }


}

